#!usr/bin/env python3
# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import redirect


def redirect_blog(request):
    return redirect('posts_list_url', permanent=True)
