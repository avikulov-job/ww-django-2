#!usr/bin/env python3
# -*- coding: utf-8 -*-

from django.urls import reverse
from django.shortcuts import (render, redirect)

from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Q

from .models import (Post, Tag)
from .forms import (PostForm, TagForm)
from .utils import (
    ObjectCreateMixin, ObjectDetailMixin, ObjectUpdateMixin, ObjectDeleteMixin)


# Create your views here.
class SearchBlog(View):
    cur_sep = '::'
    per_page = 10

    def __init__(self, *args, **kwargs):
        self.query_full = None
        self.object_typename = None
        self.query_simple = None
        self.page_num = 1

        super(SearchBlog, self).__init__(*args, **kwargs)

    def get(self, request):
        self.query_full = request.GET.get('q', '')
        self.page_num = request.GET.get('p', 1)

        # sq - search query
        if not self.query_full:
            return redirect(reverse('posts_list_url'))

        sq_list = self.query_full.split(self.cur_sep)
        if len(sq_list) > 1:
            self.query_simple = self.cur_sep.join(sq_list[1:])
            self.object_typename = sq_list[0].strip().lower()

            all_models = self._get_available_models_typenames()
            model = all_models.get(self.object_typename, None)

            if model is not None:
                return self.s_detail(request)

            return self.s_main(request)

        self.object_typename = None
        self.query_simple = sq_list[0]
        return self.s_main(request)

    def s_main(self, request):
        all_models = self._get_available_models_typenames()

        models_data = dict()
        for model_name in all_models.keys():
            model = all_models[model_name]
            mfilter = self._get_models_filter(model_name, self.query_simple)

            objects_count = model.objects.filter(mfilter).count()
            if objects_count < 1:
                continue

            models_data[model_name] = {
                'url': f'?q={model_name}{self.cur_sep}{self.query_simple}',
                'count': objects_count,
            }

        # no results
        results_num = len(models_data.keys())
        if results_num == 0:
            return self._render_no_results_page(request)

        # redirect to "search-detail"
        elif results_num == 1:
            self.object_typename = list(models_data.keys())[0]
            return self.s_detail(request)

        # two or more results
        elif results_num > 1:
            template = 'blog/search_blog_main.html'
            context = {'models_data': models_data}
            return render(request, template, context=context)

        # negative number of results?
        else:
            raise Exception(
                f'Expected results number >= 0, but have: {results_num}!')

        return None

    def s_detail(self, request):
        all_models = self._get_available_models_typenames()

        model = all_models[self.object_typename]
        mfilter = self._get_models_filter(self.object_typename, self.query_simple)

        objs = model.objects.filter(mfilter)
        objs_count = objs.count()

        # no results
        if objs_count == 0:
            return self._render_no_results_page(request)

        # redirect to this element personal page
        elif objs_count == 1:
            obj = objs.first()
            return redirect(obj.get_absolute_url())

        elif objs_count > 1:
            paginator_obj = Paginator(objs, self.per_page)
            template = 'blog/search_blog_detail.html'
            return self._render_with_paginator(request, paginator_obj, template)

        # objects found count < 0?
        else:
            raise Exception(
                f'Expected objects count >= 0, but have: {objs_count}')

        return None

    def _render_with_paginator(self, request, paginator, template):

        page = paginator.get_page(self.page_num)
        is_paginated = page.has_other_pages()

        page_suffix = f'&q={self.query_full}'
        url_template = '?p={0}' + page_suffix

        prev_url = url_template.format(page.previous_page_number()) if page.has_previous() else ''
        next_url = url_template.format(page.next_page_number()) if page.has_next() else ''

        context = {
            'search_query': self.query_simple,
            'page_object': page,
            'page_suffix': page_suffix,
            'is_paginated': is_paginated,
            'prev_url': prev_url,
            'next_url': next_url,
        }

        return render(request, template, context=context)

    def _render_no_results_page(self, request):
        template = 'blog/search_blog_no_results.html'
        context = {'search_query': self.query_simple}

        return render(request, template, context=context)

    @staticmethod
    def _get_available_models_typenames():
        result = {
            'post': Post,
            'tag': Tag,
        }

        return result

    def _get_models_filter(self, typename, query):
        all_typenames = self._get_available_models_typenames()
        if all_typenames.get(typename, None) is None:
            raise ValueError(f'Typename must be one of: {all_typenames.keys()}')

        if typename == 'post':
            p_title = Q(title__icontains=query)
            p_body = Q(body__icontains=query)
            return (p_title | p_body)

        elif typename == 'tag':
            t_title = Q(title__icontains=query)
            return t_title

        return None


def posts_list(request):
    posts = Post.objects.all()

    paginator = Paginator(posts, 1)
    page_num = request.GET.get('p', 1)
    page = paginator.get_page(page_num)

    is_paginated = page.has_other_pages()
    prev_url = f'?p={page.previous_page_number()}' if page.has_previous() else ''
    next_url = f'?p={page.next_page_number()}' if page.has_next() else ''

    context = {
        'page_object': page,
        'is_paginated': is_paginated,
        'prev_url': prev_url,
        'next_url': next_url,
    }

    return render(request, 'blog/index.html', context=context)


def tags_list(request):
    tags = Tag.objects.all()
    return render(request, 'blog/tags_list.html', context={'tags': tags})


class TagDetail(ObjectDetailMixin, View):
    model = Tag
    template = 'blog/tag_detail.html'


class TagCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    form_model = TagForm
    template = 'blog/tag_create.html'

    # Login required mixin
    raise_exception = True


class TagUpdate(LoginRequiredMixin, ObjectUpdateMixin, View):
    model = Tag
    form_model = TagForm
    template = 'blog/tag_update.html'

    # Login required mixin
    raise_exception = True


class TagDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Tag
    template = 'blog/tag_delete.html'
    redirect_url = 'tags_list_url'

    # Login required mixin
    raise_exception = True


class PostDetail(ObjectDetailMixin, View):
    model = Post
    template = 'blog/post_detail.html'


class PostCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    form_model = PostForm
    template = 'blog/post_create.html'

    # Login required mixin
    raise_exception = True


class PostUpdate(LoginRequiredMixin, ObjectUpdateMixin, View):
    model = Post
    form_model = PostForm
    template = 'blog/post_update.html'

    # Login required mixin
    raise_exception = True


class PostDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Post
    template = 'blog/post_delete.html'
    redirect_url = 'posts_list_url'

    # Login required mixin
    raise_exception = True
