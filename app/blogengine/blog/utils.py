#!usr/bin/env python3
# -*- coding: utf-8 -*-


from django.urls import reverse
from django.shortcuts import (
    render, redirect, get_object_or_404)


class ObjectDetailMixin(object):
    model = None
    template = None

    def get(self, request, slug):
        obj = get_object_or_404(self.model, slug__iexact=slug)

        context = {
            self.model.__name__.lower(): obj,
            'admin_object': obj,
            'detail': True,
        }

        return render(request, self.template, context=context)


class ObjectCreateMixin(object):
    form_model = None
    template = None

    def get(self, request):
        form = self.form_model()
        return render(request, self.template, context={'form': form})

    def post(self, request):
        bound_form = self.form_model(request.POST)

        if bound_form.is_valid():
            new_tag = bound_form.save()
            return redirect(new_tag)

        return render(request, self.template, context={'form': bound_form})


class ObjectUpdateMixin(object):
    model = None
    form_model = None
    template = None

    def get(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        bound_form = self.form_model(instance=obj)

        context = {'form': bound_form,
                   self.model.__name__.lower(): obj}

        return render(request, self.template, context=context)

    def post(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        bound_form = self.form_model(request.POST, instance=obj)

        if bound_form.is_valid():
            new_obj = bound_form.save()
            return redirect(new_obj)

        context = {'form': bound_form,
                   self.model.__name__.lower(): obj}

        return render(request, self.template, context=context)


class ObjectDeleteMixin(object):
    model = None
    template = None
    redirect_url = None

    def get(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        return render(request, self.template, context={self.model.__name__.lower(): obj})

    def post(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        obj.delete()

        # redirecting to another page.
        return redirect(reverse(self.redirect_url))
