#!usr/bin/env python3
# -*- coding: utf-8 -*-


from django import forms
from django.core.exceptions import ValidationError

from .models import (Post, Tag)


class TagForm(forms.ModelForm):
    class Meta(object):
        model = Tag
        fields = ['title', 'slug']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()

        if new_slug == 'create':
            raise ValidationError('Slug may not be "Create"!')

        if Tag.objects.filter(slug__iexact=new_slug).count():
            raise ValidationError(
                f'Slug must be unique. We have "{new_slug}" slug already.')

        return new_slug


class PostForm(forms.ModelForm):
    class Meta(object):
        model = Post
        fields = ['title', 'slug', 'body', 'tags']

        attrs_txt_input = {'class': 'form-control'}
        widgets = {
            'title': forms.TextInput(attrs=attrs_txt_input),
            'slug': forms.TextInput(attrs=attrs_txt_input),
            'body': forms.Textarea(attrs=attrs_txt_input),
            'tags': forms.SelectMultiple(attrs=attrs_txt_input),
        }

    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()

        if new_slug == 'create':
            raise ValidationError('Slug may not be "Create"!')

        if Post.objects.filter(slug__iexact=new_slug).count():
            raise ValidationError(
                f'Slug must be unique. We have "{new_slug}" slug already.')

        return new_slug
